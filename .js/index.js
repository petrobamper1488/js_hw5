function createNewUser() {
    let firstName;
        do {
            firstName = prompt("Введіть Ваше ім'я:");
        } while (!/^[\p{L}\s']+$/u.test(firstName));

    let lastName;
        do {
            lastName = prompt("Введіть Ваше прізвище:");
        } while (!/^[\p{L}\s']+$/u.test(lastName));
  
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        }
    };
    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());